/// <reference types="cypress" />

describe('various tests', () => {

  beforeEach(() => {
    cy.visit("http://localhost:3000")
  })

  it('can visit "Bibliotek" page ', () => {

    cy.contains('a', /Bibliotek/i).click();

    cy.url().should('include', '/library')
    cy.contains(/Hitta nått att läsa/i).should('be.visible');

  })

  it('can change color of heart when favoriting a book', () => {

    cy.get('.heart-button').eq(0).as('heart');
    cy.get('@heart').find('path').should('have.css', 'color', 'rgb(128, 128, 128)',)

    cy.get('@heart').click()
    cy.get('@heart').find('path').should('not.have.css', 'color', 'rgb(128, 128, 128)')

  })

  it('can open and close audio modal', () => {

    cy.get('.play-button').eq(0).click();
    cy.get('audio').should('be.visible')
    cy.contains('button', /close/i).click()
    cy.get('audio').should('not.exist');

  })


})