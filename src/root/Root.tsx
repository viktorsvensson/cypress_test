import * as React from "react";
import { useState } from "react";
import { Router, Routes, Route } from "react-router-dom";
import Header from "../books/Header";
import Library from "../books/libraryView/Library";
import Main from "../books/mainView/Main";
import Player from "../books/playerView/Player";



export default function Root() {


  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="/library" element={<Library />} />
        <Route path="/main" element={<Main />} />
        <Route path="/player/:id" element={<Player/>} />
      </Routes>
    </>
  );
}
