interface IBook {
    id: number;
    title: string;
    author: string;
    liked: boolean;
  }
  
  interface IAudio {
    audioUrl: string;
    title: string;
    author: string;
  }

  export type {IBook, IAudio}