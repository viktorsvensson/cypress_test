import {Link } from "react-router-dom";

function Header() {
  return (
    <header>
      <div className="hero-header">
        <nav>
          <Link to="/library">Bibliotek</Link> |
          <Link to="/main">Mina böcker</Link> |
          <Link to="/player">Spelare</Link>
        </nav>
      </div>
    </header>
  );
}

export default Header;
