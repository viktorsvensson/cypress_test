import React, { useState } from "react";
import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { IAudio } from "../Interfaces";

interface Props {
  open: boolean;
  onClose: () => void;
  audioBook: IAudio;
}

const AudioPlayerDialog: React.FC<Props> = ({ open, onClose, audioBook }) => {
 
 
 
  const [isPlaying, setIsPlaying] = useState(false);

  return (
    <Dialog open={open} onClose={onClose}>  
    <div className="audio-player">
    <div className="close-button">
        <Button onClick={onClose}>{isPlaying ? "Pause" : "Close"}</Button>
        </div>
        <Typography variant="h5">{audioBook.title}</Typography>
        <Typography variant="subtitle1">{audioBook.author}</Typography>
        <hr></hr>
        <audio
          src={audioBook.audioUrl}
          controls={true}
          onPlay={() => setIsPlaying(true)}
          onPause={() => setIsPlaying(false)}
        />
      </div>
   
 
    </Dialog>
  );
};

export default AudioPlayerDialog;
