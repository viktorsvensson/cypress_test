import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { IBook } from "../../Interfaces";





interface Props {
  handleLiked: (id: number) => void;
  book: IBook;
}

function LibraryBook(props: Props) {

  return (
    <>
      <Card sx={{ minWidth: 120, }} className="li-book">
        <CardContent className="library-book">
          <Typography sx={{ fontSize: 15 }}>{props.book.title}</Typography>
        </CardContent>
        <CardActions className="actions" style={{padding:2}}>
        <CardContent className="library-author">
           <Typography sx={{ fontSize: 15 }}>{props.book.author}</Typography>
        </CardContent>
        <div className="liked-icon">
          <Button  size="small" >
            <FavoriteIcon
              style={{
                color: props.book.liked ? "#ed6c02" : "grey",
              }}
              onClick={() => {
                props.handleLiked(props.book.id);
              }}
            />
          </Button>
          </div>
        </CardActions>
      </Card>
    </>
  );
}

export default LibraryBook;
