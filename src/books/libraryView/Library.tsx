import { useState } from "react";
import { IBook } from "../Interfaces";
import LibraryBook from "./components/LibraryBook";

function Library() {
  const [books, setBooks] = useState<IBook[]>([
    {
      id: 1,
      title: "Gösta Berling",
      author: "Selma Lagerlöf",
      liked: false,
    },
    {
      id: 2,
      title: "Winter",
      author: "Ali Smith",
      liked: false,
    },
    {
      id: 3,
      title: "The Catcher in the Rye",
      author: "J.D. Salinger",
      liked: false,
    },
    {
      id: 1,
      title: "Gösta Berling",
      author: "Selma Lagerlöf",
      liked: false,
    },
    {
      id: 2,
      title: "Winter",
      author: "Ali Smith",
      liked: false,
    },
    {
      id: 3,
      title: "The Catcher in the Rye",
      author: "J.D. Salinger",
      liked: false,
    },
    {
      id: 1,
      title: "Gösta Berling",
      author: "Selma Lagerlöf",
      liked: false,
    },
    {
      id: 2,
      title: "Winter",
      author: "Ali Smith",
      liked: false,
    },
    {
      id: 3,
      title: "The Catcher in the Rye",
      author: "J.D. Salinger",
      liked: false,
    },
    
  ]);

  const handleLiked = (id: number) => {
    const updatedBooks = books.map((b) => {
      if (b.id === id) {
        return { ...b, liked: !b.liked };
      }
      return b;
    });
    setBooks(updatedBooks);
  };

  const bookList = books.map((book) => (
    <LibraryBook book={book} handleLiked={handleLiked} />
  ));

  return (
    <div className="bg">
      <section className="border">
        <div className="title">
          <h1>Hitta nått att läsa:</h1>
        </div>
        <div className="center">
          <div className="lib-books-container">{bookList}</div>
        </div>
      </section>
    </div>
  );
}

export default Library;
