import React, { useState } from "react";
import { IBook } from "../Interfaces";
import Book from "./components/Book";


function Main() {
  const [books, setBooks] = useState<IBook[]>([
    {
      id: 1,
      title: "Gösta Berlings saga",
      author: "Selma Lagerlöf",
      liked: false,
    },
    {
      id: 2,
      title: "Winter",
      author: "Ali Smith",
      liked: false,
    },
    {
      id: 3,
      title: "The Catcher in the Rye",
      author: "J.D. Salinger",
      liked: false,
    },
  ]);
 



  const handleLiked = (id: number) => {
    const updatedBooks = books.map((b) => {
      if (b.id === id) {
        return { ...b, liked: !b.liked };
      }
      return b;
    });
    setBooks(updatedBooks);
  };

  const bookList = books.map((book) => (
    <Book book={book} handleLiked={handleLiked} />
  ));

  return (
    <div className="bg">
      <section className="border">
        <div className="title">
          <h1>Dina böcker:</h1>
        </div>
        <div className="books-container">{bookList}</div>
        <div className="box">
          <p>
            <span className="left">"</span> du är aldrig ensam
          </p>
          <p>
            när du läser <span className="right"> " </span>
          </p>
        </div>
      </section>
    </div>
  );
}

export default Main;
