import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import PlayCircleIcon from "@mui/icons-material/PlayCircle";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import AudioPlayerDialog from "../../playerView/AudioPlayerDialog";
import { IAudio, IBook } from "../../Interfaces";





interface Props {
  handleLiked: (id: number) => void;
  book: IBook;
}

function Book(props: Props) {



  const book = { audioUrl: "", title: "Winter", author: "Ali Smith" };
  const [open, setOpen] = useState(false);
  const [audioBook, setAudioBook] = useState<IAudio>(book);



  const handlePlay = (book: IBook) => {
    setOpen(true);
    setAudioBook({ audioUrl: "", title: book.title, author: book.author });
  };

  const handleCloseDialog = () => {
    setOpen(false);
  };

  return (
    <>
      <Card className="book">
        <CardContent className="book-card-img">
          <Typography sx={{ fontSize: 20 }}>{props.book.title}</Typography>
          <Typography sx={{ fontSize: 15 }}>{props.book.author}</Typography>
        </CardContent>
        <CardActions className="actions">
          <Button className="play-button" onClick={() => handlePlay(props.book)}>
            <PlayCircleIcon className="color" />
          </Button>
          <Button className="heart-button" size="small">
            <FavoriteIcon
              style={{
                color: props.book.liked ? "#ed6c02" : "grey",
              }}
              onClick={() => {
                props.handleLiked(props.book.id);
              }}
            />
          </Button>
        </CardActions>
      </Card>
      <AudioPlayerDialog
        open={open}
        onClose={handleCloseDialog}
        audioBook={audioBook}
      />
    </>
  );
}

export default Book;
