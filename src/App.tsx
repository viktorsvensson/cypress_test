import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Root from "./root/Root";

function App() {
  return (
    <div className="App">
      <Root/>
    </div>
  );
}

export default App;
